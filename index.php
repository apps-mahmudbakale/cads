
<?php 
session_start();
require_once 'inc/connection.php'; 
require_once 'inc/class.validation.php';
require_once 'inc/functions.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>medicals</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="css/css.css">
	<link rel="stylesheet" type="text/css" href="css/css1.css">
	 <link rel="stylesheet"  href="fontawesome/web-fonts-with-css/css/fontawesome-all.min.css">
</head>
<body>
	<!-- for the top of the webpage-->
 <div class = "header">
    <h2>
    <i class = "cads"><i class="fa fa-stethoscope"></i> CADS </i><i class = "remainder"> Computer Assisted Diagnostic System </i>
    </h2>
</div>

<!-- login -->
<div class="container" style="">
				
            <div class="col-lg-4 col-lg-offset-4">
              <div class="panel panel-success">
              <div class="panel-heading"></div>
		        <div class="panel-body">
		        	<!-- <img  src="images/avatar1.png"> -->

              <?php 

                if (isset($_POST['login'])) {
                    $username = $_POST['username'];
                    $password = SHA1($_POST['password']);
            	$fields = array(
                array('name'=>'username',
                      'app_name' => 'Username',
                      'isRequired' => true
                     ),
                 array('name'=>'password',
                      'app_name' => 'Password',
                      'isRequired' => true
                     )
            );
		$Validation = new Validation($fields,'POST');
		if($Validation->out == 1) {
     $query = mysqli_query($db,"SELECT * FROM admin WHERE username='$username' AND password='$password'");
     $row = mysqli_fetch_array($query);
     $dquery = mysqli_query($db,"SELECT * FROM doctors WHERE username ='$username' AND password='$password'");
     $drow = mysqli_fetch_array($dquery);

                if ($row >= 1) {
                   $_SESSION['admin'] = $row['username'];
                    header('Location:dashboard.php');
                }elseif ($drow >= 1) {
                    $_SESSION['doctor'] = $drow['username'];
                    $_SESSION['doctor_id'] = $drow['doctor_id'];
                    header('Location:homepage.php');
                }

        }else{
        	errorArray($Validation->errors);
        }
}

               ?>
			
			<form method="post">
				 <label>Username</label>
				 <input type='text' class='form-control' name='username' />
				 <label>Password</label>
				 <input type='password' class='form-control' name='password' />
				 <hr/>
				 <button type="submit" class='btn btn-success' name='login'> Sign in</button>
			</form>
		</div>
              </div>
              </div>
          </div>
<!--end of login for student -->

</body>
</html>