-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 07, 2020 at 07:33 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cads`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `username`, `password`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997');

-- --------------------------------------------------------

--
-- Table structure for table `differentials`
--

CREATE TABLE `differentials` (
  `differential_id` int(11) NOT NULL,
  `symptom_id` int(11) NOT NULL,
  `disease_id` int(11) NOT NULL,
  `differential` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `differentials`
--

INSERT INTO `differentials` (`differential_id`, `symptom_id`, `disease_id`, `differential`) VALUES
(1, 1, 1, 'Regular coughing'),
(2, 1, 2, 'Cough lasts more than three weeks'),
(3, 1, 2, 'Cough that prevents you from sleeping'),
(4, 1, 9, 'Chronic cough'),
(5, 3, 2, 'Slight fever and chills'),
(6, 5, 2, 'Mucus is white, yellowish-gray or green'),
(8, 5, 1, 'Mucus is clear'),
(10, 5, 9, 'Mucus is yellowish-gray or bloody');

-- --------------------------------------------------------

--
-- Table structure for table `disease`
--

CREATE TABLE `disease` (
  `disease_id` int(11) NOT NULL,
  `disease_name` text NOT NULL,
  `age_range` varchar(60) NOT NULL,
  `inheritable` varchar(60) NOT NULL,
  `desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disease`
--

INSERT INTO `disease` (`disease_id`, `disease_name`, `age_range`, `inheritable`, `desc`) VALUES
(1, 'Asthma', 'All ages', 'Yes', 'Asthma is an inflammatory condition which affects the airways. It leads to sudden reversible airway blockage and reduced airflow into the lungs. Air pollution and allergens may be triggers for asthma attacks. Asthma can be made worse by exposure to cigarette smoke, airway infections or by allergies. The main risk factors are genetic (having a blood relative with the condition) and environmental. Asthma affects both children and adults.'),
(2, 'Acute Bronchitis', 'All ages', 'No', 'Acute bronchitis is a short-term inflammation of the bronchial tubes, which carry air into the lungs. Viruses, bacteria or irritants can lead to inflammation of the se airways, causing difficulties inhaling sufficient air. If the bronchitis infection is due to a virus it can easily be spread through the air by emitting droplets when talking or coughing or by other close physical contact. Acute bronchitis is a common condition that can affect anyone at any age but the elderly and infants are at a slightly higher risk. Acute bronchitis is most often caused by the same viruses that cause a flu or a cold, and may come on following an episode of a cold or the flu. Sometimes a bacterial infection can lead to acute bronchitis.'),
(9, 'COPD', '40+ years', 'Yes', 'Chronic obstructive pulmonary disease (COPD), is a condition where the lungâ€™s small passageways called bronchioles become narrowed and blocked, leading to poor airflow. COPD is a progressive condition, which means it develops slowly but constantly if not treated. It mostly affects elderly people that have a long history of smoking tobacco. Young non-smokers developing this condition may suffer from a hereditary form of COPD. People with COPD suffer from shortness of breath and are more prone to infections of the respiratory tract. Most people with COPD will have periods during which their symptoms get worse. These periods are called acute exacerbations of COPD and can be triggered by infections, smoking and severe airflow limitations, among other factors.'),
(10, 'Pneumonia', 'All ages', 'Yes', 'Pneumonia is an infection that affects the air sacs of the lungs. The infection can be caused by a virus, by bacteria, or by other infective causes. People who are at increased risk of pneumonia include pregnant women, people older than 65, and people with other medical conditions. Some medical conditions, especially lung conditions and immune system conditions, make people more susceptible to pneumonia, and these people may experience recurrent pneumonias. Viruses are a common cause of pneumonia. Bacterial causes are less common and other causes are even rarer.');

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `doctor_id` int(11) NOT NULL,
  `firstname` varchar(60) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(60) NOT NULL,
  `speciality` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`doctor_id`, `firstname`, `lastname`, `phone`, `email`, `speciality`, `username`, `password`) VALUES
(1, 'Zeenat', 'Lawal', '08067556655', 'zeenatlawal82@gmail.com', 'General Practitioner', 'zee', '7c4a8d09ca3762af61e59520943dc26494f8941b'),
(2, 'Maryam', 'Suleiman', '08038349108', 'maryamsuleiman@gmail.com', 'Pediatrician', 'Drmar', 'df8052bb43fbdafb45b5ab52ccf5cf86d3918d31'),
(3, 'Aminat', 'Lawal', '08165828405', 'aminlaw@gmail.com', 'Surgeon', 'Amal', '40b8c810390d37f99e3c3443ac251315dcb11a85'),
(5, 'Aisha', 'Bello Umar', '08036598180', 'Ayshermury@gmail.com', 'Consultant', 'ashuk', '573f307c07d3bd3d2f52dec7666e566ec1f85661');

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `patient_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `patientno` varchar(12) NOT NULL,
  `age` varchar(10) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `gender` varchar(60) NOT NULL,
  `status` varchar(60) NOT NULL,
  `doctor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`patient_id`, `name`, `patientno`, `age`, `phone`, `gender`, `status`, `doctor_id`) VALUES
(8, 'Adam Suleiman', 'CADS-2423', '13', '08045367892', '', '', 0),
(11, 'Aminu Umar', 'CADS-028', '76', '08043762819', '', '', 0),
(12, 'Abdulsalam Muhammad', 'CADS-2322', '25', '08072871645', '', '', 0),
(15, 'Mudassir Halado', 'CADS-22023', '22', '08038969767', 'Male', 'Married', 0),
(16, 'Hadiza Batsari', 'CADS-03332', '23', '08025152258', 'Female', 'Married', 1);

-- --------------------------------------------------------

--
-- Table structure for table `symptoms`
--

CREATE TABLE `symptoms` (
  `symptom_id` int(11) NOT NULL,
  `disease_id` int(11) NOT NULL,
  `symptom` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `symptoms`
--

INSERT INTO `symptoms` (`symptom_id`, `disease_id`, `symptom`) VALUES
(1, 1, 'Coughing'),
(2, 2, 'Headache/muscle pain'),
(3, 2, 'Fever'),
(4, 9, 'Blue fingernails/lips'),
(5, 2, 'Production of mucus'),
(6, 10, 'Confusion or delirium'),
(7, 10, 'Dusky or purplish skin colour'),
(8, 2, 'Runny/stuffy nose'),
(10, 9, 'Swollen legs, feet or ankle'),
(11, 1, 'Recurrent respiratory tract infections such as cold or flu'),
(12, 1, 'Chest tightness or pain');

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE `temp` (
  `temp_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `symptom_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp`
--

INSERT INTO `temp` (`temp_id`, `patient_id`, `symptom_id`) VALUES
(1, 8, 1),
(2, 8, 3),
(3, 8, 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `differentials`
--
ALTER TABLE `differentials`
  ADD PRIMARY KEY (`differential_id`);

--
-- Indexes for table `disease`
--
ALTER TABLE `disease`
  ADD PRIMARY KEY (`disease_id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`doctor_id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `symptoms`
--
ALTER TABLE `symptoms`
  ADD PRIMARY KEY (`symptom_id`);

--
-- Indexes for table `temp`
--
ALTER TABLE `temp`
  ADD PRIMARY KEY (`temp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `differentials`
--
ALTER TABLE `differentials`
  MODIFY `differential_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `disease`
--
ALTER TABLE `disease`
  MODIFY `disease_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `doctor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `symptoms`
--
ALTER TABLE `symptoms`
  MODIFY `symptom_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `temp`
--
ALTER TABLE `temp`
  MODIFY `temp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
