
<?php 
session_start();
require_once 'inc/connection.php'; 
require_once 'inc/class.validation.php';
require_once 'inc/functions.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Doctor | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <style type="text/css">
     .header{
            width:100%;
            margin-left:200px;
        }
     .cads{
            font-style: normal;
            font-size: 2.9em;
            color: #99CCFF;
            text-decoration: underline;
            text-shadow: 2px 2px 2px gray;
        }
      .remainder{
            font-style: normal;
            font-size: 1.2em;
            color: purple;
            position: relative;
            top: -15px;
            left: -20px;
            text-shadow: 2px 2px 2px gray;
        }
  </style>
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class = "header">
    <h2>
    <i class = "cads"> CADS </i><i class = "remainder"> Computer Assisted Diagnostic System </i>
    </h2>
</div>
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="dashboard.php" class="navbar-brand"><b>C</b>ADS</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="homepage.php"><i class="fa fa-home"></i> Home <span class="sr-only">(current)</span></a></li>
            <li><a href=""><i class="fa fa-users"></i> Patients</a></li>
            <li><a href=""><i class="fa fa-asterisk"></i> Diseases</a></li>
            <li><a href=""><i class="fa fa-heartbeat"></i> Symtomps</a></li>


          </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="images/avatar1.png" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">Doctor</span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                  <p>
                    Doctor-Name
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-right">
                    <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <br>
        <div class="col-lg-12">
      <div class="panel panel-default" style='border-radius:0'>
        <div class="panel-heading"><i class="fa fa-table"></i> SYMPTOMS</div>
        <div class="panel-body">
            <table class='table table-striped col-lg-12'>
            <thead>
               <tr>
                   <th>S/N</th>
                   <th>DISEASE NAME</th>
                   <th>AGE RANGE</th>
                   <th>INHERITABLE</th>
                   <th>ACTIONS</th>
                </tr>
            </thead>
               <tbody>
                 <?php
                 $sn = 0;
                 $query = mysqli_query($db, "SELECT * FROM disease");
                 while($row = mysqli_fetch_array($query)) {
                   $sn++;
                   echo "<tr>";
                     echo "<td>".$sn."</td>";
                     echo "<td>".$row['disease_name']."</td>";
                     echo "<td>".$row['age_range']."</td>";
                     echo "<td>".$row['inheritable']."</td>";
                      echo "<td><a href='' class='btn btn-default'><i class='fa fa-edit'></i></a><a href='' class='btn btn-default'><i class='fa fa-trash'></i></a></td>";
                   echo "</tr>";
                 }
                ?>
               </tbody>
                
            </table>
        </div>
      </div>
    </div>  
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <!-- <b>Version</b> 2.4.0 -->
      </div>
      <strong>Copyright &copy; 2014-2016 <a href=""></a>.</strong> 
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
