<?php 
session_start();
require_once 'inc/connection.php'; 
require_once 'inc/class.validation.php';
require_once 'inc/functions.php';
$id = $_GET['id'];

$query = mysqli_query($db,"SELECT * FROM disease WHERE disease_id = '$id'");
$row = mysqli_fetch_array($query);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <style type="text/css">
     .header{
            width:100%;
            margin-left:200px;
        }
     .cads{
            font-style: normal;
            font-size: 2.9em;
            color: #99CCFF;
            text-decoration: underline;
            text-shadow: 2px 2px 2px gray;
        }
      .remainder{
            font-style: normal;
            font-size: 1.2em;
            color: purple;
            position: relative;
            top: -15px;
            left: -20px;
            text-shadow: 2px 2px 2px gray;
        }
        .has-error{
          border-color: #dd4b39;
          box-shadow: none;
        }
  </style>
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class = "header">
    <h2>
    <i class = "cads"> <i class="fa fa-stethoscope"></i> CADS </i><i class = "remainder"> Computer Assisted Diagnostic System </i>
    </h2>
</div>
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="dashboard.php" class="navbar-brand"><b>C</b>ADS</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="dashboard.php"><i class="fa fa-home"></i> Home </a></li>
            <li><a href="doctors.php"><i class="fa fa-stethoscope"></i> Doctors </a></li>
            <li><a href="patients.php"><i class="fa fa-users"></i> Patients</a></li>
            <li class="active"><a href="diseases.php"><i class="fa fa-asterisk"></i> Diseases</a></li>
            <li><a href="symptoms.php"><i class="fa fa-heartbeat"></i> Symptoms</a></li>


          </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="images/avatar1.png" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">Alexander Pierce</span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                  <p>
                    Alexander Pierce - Web Developer
                    <small>Member since Nov. 2012</small>
                  </p>
                </li>
                <!-- Menu Body -->
                <li class="user-body">
                  <div class="row">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <br>
       <div class="col-lg-12">
      <div class="panel panel-default" style='border-radius:0'>
        <div class="panel-heading"><i class='fa fa-folder-open'></i> New Disease</div>
        <div id="status">
          <?php 
          if(isset($_POST['save'])){
            $name = $_POST['name'];
            $range = $_POST['range'];
            $inheritable = $_POST['inheritable'];
            $desc = $_POST['desc'];
          $query = mysqli_query($db,"UPDATE `disease` SET `disease_name`='$name',`age_range`='$range',`inheritable`='$inheritable',`desc`='$desc' WHERE `disease_id`='".$_GET[id]."'");
    if ($query) {
      success('Disease Updated Successfuly');?>
      <script>
              setTimeout(function(){
                window.location='diseases.php';
              }, 7000)
            </script>
   <?php }else{
      error('Failed to Update Disease');
    }
          } ?>

        </div>
          <div class="panel-body">
          <form action="" method="POST"> 
                 <div class="col-lg-6">
                  Disease Name
                   <input type="text" name="name" value="<?php echo $row['disease_name'] ?>" class="form-control">
                   <span id="fname_text" style="color: #dd4b39"></span>
                 </div>
                  <div class="col-lg-6">
                  Age Range
                  <select name="range" name="range" class="form-control">
                     <option selected><?php echo $row['age_range'] ?></option>
                    <?php 
                    for ($i=12; $i <100; $i++) { 
                      $range = $i." - ".($i+6);
                      echo "<option>".$range."</option>";
                    }

                     ?>
                  </select>
                   <span id="range_text" style="color: #dd4b39"></span>
                 </div>
                  <div class="col-lg-6">
                  Inheritable
                   <select name="inheritable" class="form-control">
                    <option selected><?php echo $row['inheritable'] ?></option>
                     <option>Yes</option>
                     <option>No</option>
                   </select>
                   <span id="inheritable_text" style="color: #dd4b39"></span>
                 </div>
                 <div class="col-lg-12">
                   Disease Description
                   <textarea name="desc"  class="form-control"><?php echo $row['desc']; ?></textarea>
                 </div>
                
        </div>
        <div class='panel-footer'>
            <button type="submit" name="save"  class='btn btn-success'><i class="fa fa-save"></i> Save</button>
        </div>
         </form>
      </div>
    </div>
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
      <!--   <b>Version</b> 2.4.0 -->
      </div>
      <strong>Copyright &copy; 2014-2016 <a href=""></a>.</strong> 
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript">
  $(document).ready(()=>{
     $('#name').focusout(function() {
      $value = $('#name').val();
       $len = $value.length;
       if ($value === "") {
        $('#name').addClass('has-error');
        $('#fname_text').text('Disease Name is Required');
        document.getElementById('save').disabled = true;
       }else
  if ($value.match(/^[0-9]*$/)) {
    $('#name_text').text('Disease Name  must be character only');
    document.getElementById('save').disabled = true;
  }else
  if ($len < 3) 
    {
  $('#name').addClass('has-error');
    $('#name_text').text('Disease Name is too short character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  if ($len > 20) 
    {
  $('#name').addClass('has-error');
    $('#name_text').text('Disease Name is too long character length must be between 3 and 20');
    document.getElementById('save').disabled = true;
  }
  else
  {
    $('#name').removeClass('has-error').addClass('has-success');
    $('#fname_text').text('').removeClass('has-error');
    document.getElementById('save').disabled = false;
  }
     });
  
    $('#save').click(()=>{
     var name = $('#name').val();
     var range = $('#range').val();
     var inheritable = $('#inheritable').val();
     var desc = $('#desc').val();
     var id = $('#id').val();
     //alert(desc);
     $.ajax({
      type: 'POST',
      url: 'Ajax.Edit.php',
      data:{name:name,range:range,inheritable:inheritable,desc:desc,id:id},
      success: function(x) {
        $('#status').html(x);
      }
     })
    })
  })
</script>
</body>
</html>
